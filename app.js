const express = require("express");
const expressHandlebars = require("express-handlebars");
const bodyParser = require("body-parser");
const expressSession = require("express-session");
const robertDb = require("./robert-db");
const bcrypt = require("bcrypt");
const csrf = require("csurf");
const csrfProtection = csrf({ cookie: false });
const parseForm = bodyParser.urlencoded({ extended: false });
let csurfToken = "";

const myUsername = "admin";
const hash = "$2b$10$.clQw7q2fkzyjWByh.fdYequrRkCsgX06hPBtLyp/RvHXHbCi9j9O"; //hash = "123"

const app = express();

app.engine(
  ".hbs",
  expressHandlebars({
    extname: ".hbs",
    defaultLayout: "main.hbs"
  })
);

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(
  expressSession({
    secret: "memes",
    resave: false,
    saveUninitialized: false
  })
);

app.use(express.static("public"));

app.get("/", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  robertDb.getAllComments(function(error, comments) {
    if (!error.length) {
      const model = {
        comments: comments,
        isLoggedIn: request.session.isLoggedIn,
        csrfToken: csurfToken
      };
      response.render("home.hbs", model);
    } else {
      response.status(500).end();
      return;
    }
  });
});

app.post("/add-comment", function(request, response) {
  if (csurfToken === request.body._csrf) {
    const comment = request.body.comment;

    robertDb.createComment(comment, function(error) {
      if (!error.length) {
        response.redirect("/");
      } else {
        const commentError = true;
        response.redirect("/?commentError=" + commentError);
      }
    });
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/edit-comment/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    const comment = request.body.comment;
    const id = request.params.id;

    robertDb.updateCommentById(comment, id, function(error) {
      if (!error.length) {
        response.redirect("/");
      } else {
        const commentError = true;
        response.redirect("/?commentError=" + commentError);
      }
    });
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/delete-comment/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    const id = request.params.id;

    robertDb.deleteCommentById(id, function(error) {
      if (!error.length) {
        response.redirect("/");
      } else {
        const commentError = true;
        response.redirect("/?commentError=" + commentError);
      }
    });
  } else {
    response.status(403).end();
    return;
  }
});

app.get("/about", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  const model = {
    isLoggedIn: request.session.isLoggedIn,
    csrfToken: csurfToken
  };
  response.render("about.hbs", model);
});

app.get("/contact", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  const model = {
    isLoggedIn: request.session.isLoggedIn,
    csrfToken: csurfToken
  };
  response.render("contact.hbs", model);
});

app.post("/add-question", function(request, response) {
  if (csurfToken === request.body._csrf) {
    const question = request.body.question;
    const answer = "";

    robertDb.createFaq(question, answer, function(error) {
      if (!error.length) {
        response.redirect("/faqs");
      } else {
        const questionError = true;
        response.redirect("/contact/?questionError=" + questionError);
      }
    });
  } else {
    response.status(403).end();
    return;
  }
});

app.get("/faqs", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  const isLoggedIn = request.session.isLoggedIn;

  robertDb.getAllFaqs(function(error, faqs) {
    if (!error.length) {
      const model = {
        faqs: faqs,
        isLoggedIn: isLoggedIn,
        csrfToken: csurfToken
      };
      response.render("faqs.hbs", model);
    } else {
      response.status(500).end();
      return;
    }
  });
});

app.get("/answer-question/:id", csrfProtection, parseForm, function(
  request,
  response
) {
  csurfToken = request.csrfToken();
  if (request.session.isLoggedIn) {
    const id = request.params.id;
    robertDb.getFaqById(id, function(error, faq) {
      if (!error.length) {
        const model = {
          faq: faq,
          isLoggedIn: request.session.isLoggedIn,
          csrfToken: csurfToken
        };
        response.render("answer-question.hbs", model);
      } else {
        response.status(500).end();
        return;
      }
    });
  } else {
    response.redirect("/login");
  }
});

app.post("/answer-question/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    if (request.session.isLoggedIn) {
      const id = request.params.id;
      const question = request.body.question;
      const answer = request.body.answer;

      robertDb.updateFaqById(question, answer, id, function(error) {
        if (!error.length) {
          response.redirect("/faqs");
        } else {
          const questionError = true;
          response.redirect(
            "/answer-question/:id/?questionError=" + questionError
          );
        }
      });
    } else {
      response.redirect("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/delete-question/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    if (request.session.isLoggedIn) {
      const id = request.params.id;

      robertDb.deleteFaqById(id, function(error) {
        if (!error.length) {
          response.redirect("/faqs");
        } else {
          const questionError = true;
          response.redirect(
            "/answer-question/:id/?questionError=" + questionError
          );
        }
      });
    } else {
      response.render("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.get("/blog", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  robertDb.getAllBlogposts(function(error, blogposts) {
    if (!error.length) {
      const isLoggedIn = request.session.isLoggedIn;

      const model = {
        blogposts: blogposts,
        isLoggedIn: isLoggedIn,
        csrfToken: csurfToken
      };
      response.render("blog.hbs", model);
    } else {
      response.status(500).end();
      return;
    }
  });
});

app.post("/add-blogpost", function(request, response) {
  if (csurfToken === request.body._csrf) {
    if (request.session.isLoggedIn) {
      const blogpost = request.body.blogpost;

      robertDb.createBlogpost(blogpost, function(error) {
        if (!error.length) {
          response.redirect("/blog");
        } else {
          const blogError = true;
          response.redirect("/blog/?blogError=" + blogError);
        }
      });
    } else {
      response.redirect("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/edit-blogpost/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    if (request.session.isLoggedIn) {
      const id = request.params.id;
      const blogpost = request.body.blogpost;

      robertDb.updateBlogpostById(blogpost, id, function(error) {
        if (!error.length) {
          response.redirect("/blog");
        } else {
          const blogError = true;
          response.redirect("/blog/?blogError=" + blogError);
        }
      });
    } else {
      response.redirect("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/delete-blogpost/:id", function(request, response) {
  if (csurfToken === request.body._csrf) {
    if (request.session.isLoggedIn) {
      const id = request.params.id;

      robertDb.deleteBlogpstById(id, function(error) {
        if (!error.length) {
          response.redirect("/blog");
        } else {
          const blogError = true;
          response.redirect("/blog/?blogError=" + blogError);
        }
      });
    } else {
      response.redirect("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.get("/login", csrfProtection, parseForm, function(request, response) {
  csurfToken = request.csrfToken();
  const model = {
    csrfToken: csurfToken
  };
  response.render("login.hbs", model);
});

app.post("/login", function(request, response) {
  if (csurfToken === request.body._csrf) {
    const username = request.body.username;
    const password = request.body.password;

    if (myUsername === username && bcrypt.compareSync(password, hash)) {
      request.session.isLoggedIn = true;
      response.redirect("/");
    } else {
      response.redirect("/login");
    }
  } else {
    response.status(403).end();
    return;
  }
});

app.post("/logout", function(request, response) {
  if (csurfToken === request.body._csrf) {
    request.session.isLoggedIn = false;
    response.redirect("/");
  } else {
    response.status(403).end();
    return;
  }
});

app.listen(8080, function() {
  console.log("App listening on port 8080");
});

app.use(function(request, response) {
  const model = {
    isLoggedIn: request.session.isLoggedIn
  };
  response.render("404notfound.hbs", model);
});
