$(window).ready(function() {
  $(".openCommentModal").on("click", function(error) {
    const commentId = $(this).attr("comment-id");
    const commentText = $(this).attr("comment-text");
    const modalName = $(this).attr("data-toggle");

    $("#exampleModalLabel").text("Editing comment id " + commentId);
    $("#comment-text").val(commentText);
    $("#editCommentSubmit").attr("action", "/edit-comment/" + commentId);
    $(modalName).modal();
  });
});

$(window).ready(function() {
  $(".openBlogpostModal").on("click", function(error) {
    const blogpostId = $(this).attr("blogpost-id");
    const blogpostText = $(this).attr("blogpost-text");
    const modalName = $(this).attr("data-toggle");

    $("#exampleModalLabel").text("Editing comment id " + blogpostId);
    $("#blogpost-text").val(blogpostText);
    $("#editBlogpostSubmit").attr("action", "/edit-blogpost/" + blogpostId);
    $(modalName).modal();
  });
});
