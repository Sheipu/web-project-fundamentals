const sqlite3 = require("sqlite3");

const db = new sqlite3.Database("music-databse.db");

db.run(`
	CREATE TABLE IF NOT EXISTS faqs (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		question TEXT,
		answer TEXT
	)
`);

db.run(`
    CREATE TABLE IF NOT EXISTS comments (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        comment TEXT
    )
`);

db.run(`
    CREATE TABLE IF NOT EXISTS blogposts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        blogpost TEXT
    )
`);

exports.createFaq = function(question, answer, callback) {
  const query = "INSERT INTO faqs (question, answer) VALUES (?, ?)";
  const values = [question, answer];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.getAllFaqs = function(callback) {
  const query = "SELECT * FROM faqs";

  db.all(query, function(error, faqs) {
    if (error) {
      callback(["dbError"], null);
    } else {
      callback([], faqs);
    }
  });
};

exports.getFaqById = function(id, callback) {
  const query = "SELECT * FROM faqs WHERE id = ?";

  db.get(query, [id], function(error, faq) {
    if (error) {
      callback(["dbError"], null);
    } else {
      callback([], faq);
    }
  });
};

exports.updateFaqById = function(newQuestion, newAnswer, id, callback) {
  const query = "UPDATE faqs SET question = ?, answer = ? WHERE id = ?";
  const values = [newQuestion, newAnswer, id];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.deleteFaqById = function(id, callback) {
  const query = "DELETE FROM faqs WHERE id = ?";

  db.run(query, [id], function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.createComment = function(comment, callback) {
  const query = "INSERT INTO comments (comment) VALUES (?)";
  const values = [comment];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.getAllComments = function(callback) {
  const query = "SELECT * FROM comments";

  db.all(query, function(error, comments) {
    if (error) {
      callback(["dbError"], null);
    } else {
      callback([], comments);
    }
  });
};

exports.updateCommentById = function(newComment, id, callback) {
  const query = "UPDATE comments SET comment = ? WHERE id = ?";
  const values = [newComment, id];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.deleteCommentById = function(id, callback) {
  const query = "DELETE FROM comments WHERE id = ?";

  db.run(query, [id], function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.createBlogpost = function(blogpost, callback) {
  const query = "INSERT INTO blogposts (blogpost) VALUES (?)";
  const values = [blogpost];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.getAllBlogposts = function(callback) {
  const query = "SELECT * FROM blogposts";

  db.all(query, function(error, blogposts) {
    if (error) {
      callback(["dbError"], null);
    } else {
      callback([], blogposts);
    }
  });
};

exports.updateBlogpostById = function(newBlogpost, id, callback) {
  const query = "UPDATE blogposts SET blogpost = ? WHERE id = ?";
  const values = [newBlogpost, id];

  db.run(query, values, function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};

exports.deleteBlogpstById = function(id, callback) {
  const query = "DELETE FROM blogposts WHERE id = ?";

  db.run(query, [id], function(error) {
    if (error) {
      callback(["dbError"]);
    } else {
      callback([]);
    }
  });
};
